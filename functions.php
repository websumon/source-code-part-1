<?php

//*** Support Title Tag 
add_theme_support( 'title-tag' );

//*** Enqueue FlipMart Scripts
function flipmart_enqueue_scripts(){
	
		
	wp_enqueue_style( 'main', get_template_directory_uri().'/assets/css/main.css', array(), '1.0' );
	
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '3.2.0' );

	wp_enqueue_style( 'blue', get_template_directory_uri().'/assets/css/blue.css', array(), '1.0' );
	
	wp_enqueue_style( 'owl.carousel', get_template_directory_uri().'/assets/css/owl.carousel.css', array(), '1.0' );
	
	wp_enqueue_style( 'owl.transitions', get_template_directory_uri().'/assets/css/owl.transitions.css', array(), '1.0' );
	
	wp_enqueue_style( 'animate.min', get_template_directory_uri().'/assets/css/animate.min.css', array(), '1.0' );
	
	wp_enqueue_style( 'rateit', get_template_directory_uri().'/assets/css/rateit.css', array(), '1.0' );
	
	wp_enqueue_style( 'bootstrap-select.min', get_template_directory_uri().'/assets/css/bootstrap-select.min.css', array(), '1.0' );
	
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/assets/css/font-awesome.css', array(), '1.0' );
	
	
}

add_action('wp_enqueue_scripts','flipmart_enqueue_scripts');